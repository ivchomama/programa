package programa;

public class hero {
 int posx;
 int posy;
private String skin;
 hero(int x, int y) {
		posx = x;
		posy = y;
	}
	
	void move(char c, map m) {
		switch(c) {
			case 'w': posy--; break;
			case 's': posy++; break;
			case 'a': posx--; break;
			case 'd': posx++; break;
		}
		if(posx < 0) {
			posx = m.sizex-1;
		}
		else if(posy < 0) {
			posy = m.sizey-1;
		}
		else if(posx > m.sizex-1) {
			posx = 0;
		}
		else if(posy > m.sizey-1) {
			posy = 0;
		}
	}
	public String getSkin() {
		return skin;
		
	}
	public void setSkin(String newSkin) {
		if(newSkin.equals("red") || newSkin.equals("green")) {
			skin = newSkin;
		}
		else {
			System.out.println("choose red or green");
		}
	}
}

